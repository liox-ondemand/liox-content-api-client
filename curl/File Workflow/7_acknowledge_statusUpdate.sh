#Acknowledge a status update

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/statusupdates/BwHZOxXUSxm44-b--nK8aw/acknowledge' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "updateId": "BwHZOxXUSxm44-b--nK8aw",
    "jobId": "WfF4sGC9jd",
    "requestIds": [
        "S4SZrznPRZGfjCQ0aUd_nw"
    ],
    "statusCode": "REVIEW_TRANSLATION",
    "updateTime": "2021-03-02T07:39:13.477Z",
    "hasError": false
}
