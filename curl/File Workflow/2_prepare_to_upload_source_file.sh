# Initiates uploading file to a job

curl --location --request POST 'https://content-api.staging.lionbridge.com/v2/jobs/H5GrB7C9jk/sourcefiles?fileName=demo.xml' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "fmsFileId": "%%%FMS FILE ID%%%",
    "fmsSASToken": "%%%FMS TOKEN%%%",
    "fmsUploadUrl": "%%%FMS UPLOAD URL%%%"
}