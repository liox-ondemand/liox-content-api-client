#Archive a job

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/jobs/95CklFg5jZ/archive' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "jobId": "95CklFg5jZ",
    "jobName": "Test Job-SK",
    "description": "New test Job for testing",
    "statusCode": "COMPLETED",
    "hasError": false,
    "latestErrorMessage": "",
    "submitterId": "sp_test_user_002",
    "creatorId": "sp_test_user_002",
    "providerId": "%%%PROVIDER_ID_STRING%%%",
    "poReference": "9692",
    "dueDate": "2022-06-24T07:07:42.096Z",
    "createdDate": "2021-06-25T07:07:42.096Z",
    "modifiedDate": "2021-06-25T12:07:42.096Z",
    "archived": false,
    "shouldQuote": false,
    "providerReference": "BV-000377758",
    "siteId": "%%%SITE_ID%%%",
    "globalTrackingId": "%%%GLOBAL_TRACKING_ID%%%"
}