# Add requests to job

curl --location --request POST 'https://content-api.staging.lionbridge.com/v2/jobs/95CklFg5jZ/requests/add' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
    "requestName": "front page",
    "sourceNativeId": "/en/news/front_page",
    "sourceNativeLanguageCode": "en-US",
    "targetNativeLanguageCodes": [
        "fr-FR"
    ],
    "targetNativeIds": [
        "/fr/news/front_page"
    ],
    "sourcecontentId": "%%%SOURCE_CONTENT_ID%%%"
}'

#Response
{
    "_embedded": {
        "requests": [
            {
                "requestId": "9elt-x8WTZWETdpPUtwA2g",
                "jobId": "95CklFg5jZ",
                "requestName": "front page",
                "statusCode": "CREATED",
                "hasError": false,
                "sourceNativeId": "/en/news/front_page",
                "sourceNativeLanguageCode": "en-US",
                "targetNativeId": "/fr/news/front_page",
                "targetNativeLanguageCode": "fr-FR",
                "createdDate": "2021-06-25T07:07:42.096Z",
                "modifiedDate": "2021-06-25T07:07:42.096Z",
                "sourcecontentId": "%%%SOURCE_CONTENT_ID%%%",
            }
        ]
    }
}