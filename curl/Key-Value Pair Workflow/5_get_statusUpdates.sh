#Get all unacknowledged status updates

curl --location --request GET 'https://content-api.staging.lionbridge.com/v2/statusupdates' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "_embedded": {
        "statusupdates": [
            {
                "updateId": "BwHZOxXUSxm44-b--nK8aw",
                "jobId": "95CklFg5jZ",
                "requestIds": [
                    "9elt-x8WTZWETdpPUtwA2g"
                ],
                "statusCode": "REVIEW_TRANSLATION",
                "updateTime": "2021-06-25T09:39:13.477Z",
                "hasError": false
            },
            {
                "updateId": "owMFi7NHTCqeHOfq4Cxv2g",
                "jobId": "95CklFg5jZ",
                "requestIds": [
                    "9elt-x8WTZWETdpPUtwA2g"
                ],
                "statusCode": "IN_TRANSLATION",
                "updateTime": "2021-06-25T08:39:13.477Z",
                "hasError": false
            }
        ]
    }
}