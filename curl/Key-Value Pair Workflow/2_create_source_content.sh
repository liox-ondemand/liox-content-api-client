# create a new source content
curl --location --request POST 'https://content-api.staging.lionbridge.com/v2/jobs/95CklFg5jZ/sourcecontent' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
    "fields": [
        { "key": "field_1", "value" : "Top Stories" },
        { "key": "field_2", "value" : "Popular Now" }
    ]
}'

#Response
{
    "sourcecontentId": "%%%SOURCE_CONTENT_ID%%%"
    "fields": [{
            "key": "field_1",
            "value" : "Top Stories"
        },{
            "key": "field_2",
            "value" : "Popular Now"
    }]
}
