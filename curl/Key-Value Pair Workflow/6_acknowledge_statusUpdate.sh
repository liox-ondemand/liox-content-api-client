#Acknowledge a status update

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/statusupdates/BwHZOxXUSxm44-b--nK8aw/acknowledge' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "updateId": "BwHZOxXUSxm44-b--nK8aw",
    "jobId": "95CklFg5jZ",
    "requestIds": [
        "9elt-x8WTZWETdpPUtwA2g"
    ],
    "statusCode": "REVIEW_TRANSLATION",
    "updateTime": "2021-06-25T09:50:13.477Z",
    "hasError": false
}
