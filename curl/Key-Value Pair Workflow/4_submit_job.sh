# Send job for translation

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/jobs/WfF4sGC9jd/submit' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
}'


#Response

{
    "jobId": "95CklFg5jZ",
    "jobName": "Test job SK-SP QA-9th Feb2021",
    "description": "New test Job for testing",
    "statusCode": "SENDING",
    "hasError": false,
    "submitterId": "sp_test_user_002",
    "creatorId": "sp_test_user_002",
    "providerId": "%%%PROVIDER_ID_STRING%%%",
    "poReference": "9692",
    "dueDate": "2022-06-24T07:07:42.096Z",
    "createdDate": "2021-06-25T07:07:42.096Z",
    "modifiedDate": "2021-06-25T08:07:42.096Z",
    "submittedDate": "2021-06-25T08:07:42.096Z",
    "archived": false,
    "shouldQuote": false,
    "siteId": "%%%SITE_ID%%%",
    "globalTrackingId": "%%%GLOBAL_TRACKING_ID%%%"
}
