# Approve translated content of specified request(s)

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/jobs/95CklFg5jZ/requests/approve' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
    "requestIds": [
        "9elt-x8WTZWETdpPUtwA2g"
    ]
}'

#Response

{
    "_embedded": {
        "requests": [
            {
                "requestId": "9elt-x8WTZWETdpPUtwA2g",
                "jobId": "95CklFg5jZ",
                "requestName": "front page",
                "statusCode": "TRANSLATION_APPROVED",
                "hasError": false,
                "sourceNativeId": "/en/news/front_page",
                "sourceNativeLanguageCode": "en-US",
                "targetNativeId": "/fr/news/front_page",
                "targetNativeLanguageCode": "fr-FR",
                "createdDate": "2021-06-25T07:07:42.096Z",
                "modifiedDate": "2021-06-25T10:07:42.096Z",
                "providerReference": "BV-000377758",
                "sourcecontentId": "%%%SOURCE_CONTENT_ID%%%"
            }
        ]
    }
}

