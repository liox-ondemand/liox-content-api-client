#Get target content

curl --location --request GET 'https://content-api.staging.lionbridge.com/v2/jobs/95CklFg5jZ/requests/9elt-x8WTZWETdpPUtwA2g/retrieve' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

[{
    "requestId": "9elt-x8WTZWETdpPUtwA2g",
    "sourceNativeId": "/en/news/front_page",
    "sourceNativeLanguageCode": "en-US",
    "targetNativeLanguageCode": "fr-FR",
    "targetContent": [{
            "key": "field_1",
            "value": "Xxx Xxxxxxx"
        }, {
            "key": "field_2",
            "value": "Xxxxxxx Xxx"
        }
    ]
}}]