curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' -d '{
  "requestName": "sample file content",
  "sourceNativeId": "content page/english/sample",
  "sourceNativeLanguageCode": "en",
  "targetNativeIds": [
    "content page/spanish/sample"
  ],
  "targetNativeLanguageCodes": [
    "es"
  ],
  "wordCount": 0,
  "fileId": "fa772816-2566-4b2a-83c6-19d8d073b31f"
}' 'https://content-api.lionbridge.com/v1/jobs/3a197821-7675-4444-89d0-e67bc274df69/requests/addfile' -o output.json

# [
  # {
    # "requestId": "29c9688b-1c63-40df-9214-84d6eb32e5b9",
    # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
    # "requestName": "sample file content",
    # "statusCode": {
      # "statusCode": "PENDING"
    # },
    # "hasError": false,
    # "sourceNativeId": "content page/english/sample",
    # "sourceNativeLanguageCode": "en",
    # "targetNativeId": "content page/spanish/sample",
    # "targetNativeLanguageCode": "es",
    # "createdDate": "2017-12-20T20:37:35.317Z",
    # "modifiedDate": "2017-12-20T20:37:35.317Z",
    # "wordCount": 0,
    # "fileId": "fa772816-2566-4b2a-83c6-19d8d073b31f",
    # "fileType": "text/plain"
  # }
# ]
