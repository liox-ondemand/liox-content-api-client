#### Send API requests with cURL ####

There are two sets of scripts, developed to run cURL in bash.  Each set corresponds to the steps outlined in the workflows document, on http://developers.lionbridge.com/content/docs/key-concepts.html

Each script includes comments following the cURL command(s), listing sample output.  Some strings in the scripts are marked with %%%.  They need to be replaced with values obtained from the API as setup.
